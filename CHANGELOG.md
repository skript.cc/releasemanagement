# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2019-02-28
### Fixed
- Fixed some broken links in presentation

## [0.2.0] - 2019-02-25
### Added
- Separation between presentation info and detailed information
- Git flow release tip

### Fixed
- Base url for vuepress
- Git flow instructions for publishing branches to origin

## 0.1.0 - 2019-02-24
### Added
- Deployment to github pages
- Vuepress for generating static html
- Markdown files about release management for presentation and documentation
- CHANGELOG file to keep track of changes
- README file with a short description

[Unreleased]: https://gitlab.com/skript.cc/releasemanagement/compare/0.2.0...develop
[0.2.0]: https://gitlab.com/skript.cc/releasemanagement/compare/0.1.0...0.2.0