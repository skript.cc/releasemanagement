# How does release management work?

## Preconditions

It´s as simple as this:

1. Decide on a version scheme
2. Decide on a repository branching strategy & work flow
3. Keep a changelog
4. Stick to it!

## In practice

When you have collected new features to roll out or want to apply a fix:

1. Start a release (following [git flow](how-to-gitflow.md))
2. Prep the release
    - Bump the version number (following [semantic versioning](how-to-semver.md))
    - Update the changelog (following the [keep a changelog format](how-to-keepachangelog.md))
3. Finish the release (following [git flow](how-to-gitflow.md))