# Why the fuck should I care?

## If you want to prevent this...

More than a year ago this was the situation with Liberty:

- No overview of important changes between releases
- No version locking in projects
- Dependent code (projects) stops working after updates (of Liberty)
- Unhappy developers & unhappy customers

## ...and profit from better maintainable projects

These were the gains after introducing (a better form) of release management:

- Dependency management and version locking: keeping dependent software (projects) maintainable
- Separating stable code from code in development
- Keeping track of notable changes between releases

Summarized in one word: **maintainability**