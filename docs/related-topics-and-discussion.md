# Related topics and dicussion

## Discussion
- Does semantic versioning makes sense for projects?
- Are [github flow](http://scottchacon.com/2011/08/31/github-flow.html) and/or [gitlab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/) simpler alternatives for git flow?

## Related topics
- How to do release management during the development phase of a project, and 
  integrate it more tightly with scrum.
- Generating changelogs from git commit messages
    - [Generating a changelog from git history](https://blog.mozilla.org/webdev/2016/07/15/auto-generating-a-changelog-from-git-history/)
    - [Semantic release](https://github.com/semantic-release/semantic-release)
- Make a release schedule and roadmap to plan releases and support lifetime in 
  advance. The [neos project](https://www.neos.io/download-and-extend/release-process.html) has a good one.