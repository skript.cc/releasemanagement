# Starting and finishing releases with Git flow

## 1. Decide whether to hotfix or to release

- Start a hotfix to quickly fix production problems
- Start a release to roll out new features and changes

## 2a. Applying fixes to current releases

```sh
# 1. Use semver to determine the version number

# 2. Create a hotfix branch from stable code on master branch.  
git flow hotfix start [version]

# 3. Apply fix(es) and prep release.
...

# 4. Merge your fix back into the stable (master branch) and development (develop branch) code.  
git flow hotfix finish [version]

# 5. Publish your hotfix release  
git push origin master

# 6. Make your hotfix available in the next release  
git push origin develop

# 7. Don't forget to push your tags!  
git push --tags
```

## 2b. Roll out new features and changes

```sh
# 1. Use semver to determine the version number

# 2. Create a release branch from development code on develop branch  
git flow release start [version]

# 3. Prep release (bump version, update changelog), last minute fixes
...

# 4. Merge your fix back into the stable (master branch) and development (develop branch) code.  
git flow release end [version]

# 5. Publish your release  
git push origin master

# 6. Make your release available in the next release  
git push origin develop

# 7. Don't forget to push your tags!  
git push --tags
```

::: tip
Bump your version number to an unstable (pre-release) version after merging your 
release back to develop and before publishing it to origin. If there are automatic 
builds started which rely on this version number, code on development won't be 
distributed as a stable package.
:::


## Detailed information

Git flow is a popular [branching model](https://nvie.com/posts/a-successful-git-branching-model/)
with a very handy [command line tool](https://github.com/petervanderdoes/gitflow-avh/)
and is often integrated in other Git clients.

### Why is it helpful (for release management)?

- Separates stable code from development code
- It marks changes and the state of your code in git history

<div class="note">

Git flow is especially helpful for starting new releases and doing maintenance
fixes while continuing development at the same time. It prevents you from
including unfinished code in stable releases, because something suddenly needs
to be fixed. Besides that it also structures your git history and marks 
important changes.

</div>

### How does it work?

Git flow works by starting *temporary* supporting branches from two main branches
with an infinite lifetime: master and develop.

Relevant supporting branches for release management:

- Hotfix branches, which are started from master,
- release branches, which are started from develop.

### When to hotfix and when to release?

- Start a hotfix to quickly fix production problems
- Start a release to roll out new features and changes

<div class="note">

Since the HEAD of the master branch is supposed to always reflect a *production ready*
state, hotfixes are started to quickly fix production problems.

New features and changes should be staged on the develop branch. When it's time
to do a new release, you start a new release branch based on develop.

</div>

### Relation to semver

- Hotfixes usually increment PATCH
- Releases increment MINOR when there are non breaking changes
- Releases increment MAJOR when there are breaking changes
