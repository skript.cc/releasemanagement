# How to bump versions semantically

## 1. Declare a public API

> For [semver] to work, you first need to declare a public API. [...] it is 
> important that this API be clear and precise.

## 2. Increment the version

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
> - MAJOR version when you make incompatible API changes,
> - MINOR version when you add functionality in a backwards-compatible manner, and
> - PATCH version when you make backwards-compatible bug fixes.

## Detailed information

[Semantic versioning](https://semver.org/) (semver) breaks up the version in
three major parts:

> MAJOR.MINOR.PATCH

It also allows for the addition of pre-releases and buid metadata:

> MAJOR.MINOR.PATCH-prerelease+buildnr

### Why is it helpful?

> Under this scheme, version numbers and the way they change convey meaning about 
> the underlying code and what has been modified from one version to the next.

- It communicates the *type* of changes included in a new release
- It keeps dependent software maintainable

<div class="note">

Semver makes it possible to communicate the kind of changes included in a new 
release. If done right, it allows dependent software to safely update your package
without breaking anything.

</div>

### How does it work?

- Declare a public API
- Determine what has changed since the previous release and bump the version 
  number based on that

#### Declaring a public API

> For this system to work, you first need to declare a public API. [...] it is 
> important that this API be clear and precise.

<div class="note">

Uh? Right. A clear and precisely defined public API? For libraries this makes
kinda sense. There's always some part of the code that is intended to be used
in other software. If you write a react component, the 'public api' basicly 
consists out of the components (and their props) to be used in other applications.

Liberty is also written as a library to be used by other projects, which 
implement a custom frontend. Although there's definitely no such thing as a clear
and precisely defined public API in Liberty, we can usually make an educated 
guess whether a change will break something in a project or not.

But what about software that isn't shipped to developers, but to end users? What
is the public API? The user interface? Does it still make sense to use semantic
versioning for that? I don't know for sure, let's discuss this in the end.

</div>

#### Bumping the version

> Given a version number MAJOR.MINOR.PATCH, increment the:
>
> - MAJOR version when you make incompatible API changes,
> - MINOR version when you add functionality in a backwards-compatible manner, and
> - PATCH version when you make backwards-compatible bug fixes.
