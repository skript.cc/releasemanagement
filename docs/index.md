# Release management

- [Why the fuck should I care?](why-care.md)
- [How does it work?](how-does-it-work.md)
    - [Semantic versioning](how-to-semver.md)
    - [Gitflow](how-to-gitflow.md)
    - [Keep a changelog](how-to-keepachangelog.md)
- Example
- [Related topics and discussion](related-topics-and-discussion.md)