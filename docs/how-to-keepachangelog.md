# How to keep a changelog

- With each release you ensure all notable changes are added to the changelog
- During development you keep track of notable changes in the unreleased section
- Stick to the format

```md
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
### Added
- Example file

## 0.1.0 - 2019-02-24
### Added
- Markdown files about release management for presentation and documentation
- CHANGELOG file to keep track of changes
- README file with a short description
```

## Detailed information

[Keep a changelog](https://keepachangelog.com/en/1.0.0/) is a markdown based format for documenting notable changes
for each version of a project.

### Why is it helpful?

A changelog makes it easier to keep track of the notable changes made between
releases.

### How does it work?

- With each release you ensure all notable changes are added to the changelog
- During development you keep track of notable changes in the unreleased section
- Stick to the format

### Keep a changelog format

- There is an entry for each release with a date
- Dates follow the yyyy-mm-dd format
- Changes are grouped by the following types:
    - Added
    - Changed
    - Deprecated
    - Removed
    - Fixed
    - Security
- It is mentioned whether the projects follows semver or not
- There is an (optional) unreleased section at the top to track upcoming changes

Also see the [changelog](https://gitlab.com/skript.cc/releasemanagement/blob/master/CHANGELOG.md) in this repository.