module.exports = {
  base: '/releasemanagement/',
  dest: 'public',
  title: 'Release Management',
  description: 'A presentation about release management',
  themeConfig: {
    displayAllHeaders: false,
    sidebar: [
        ['/', 'Index'],
        '/why-care',
        '/how-does-it-work',
        '/how-to-gitflow',
        '/how-to-semver',
        '/how-to-keepachangelog',
        '/related-topics-and-discussion'
    ]
  }
}